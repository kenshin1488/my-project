import module.Kotik;

public class Application {

    public static void main(String[] args) {
        Kotik mursik = new Kotik();
        Kotik barsik = new Kotik(10, "Barsik", 10,"Meow");
        mursik.setKotik(11,"Mursik",14,"Meow");
        mursik.liveAnotherDay();

        if(barsik.getVoice().equals(mursik.getVoice())) { // Сравниваем у двух объектов значение 'voice'
            System.out.println("\nOur voice is similar! ");
        } else {
            System.out.println("\nIt's different. Not the same.");
        }

        int count = Kotik.getCount(); // Присваиваем переменной 'count' значение кол-ва экземпляров класса
        System.out.println("\n"+count);
    }
}
