import food.*;
import animals.*;


public class Zoo {
    public static void main(String[] args) {

        //  Carnivorous Animal;
        Lion lion = new Lion(100, 5,"Alex",15);
        Duck duck = new Duck(10,2.3, "McDonald",7);
        Bear bear = new Bear(300,7.8,"Misha",25);

        //   Herbivore Animal;
        Carp carp = new Carp(5,0.5,"Chris",3);
        Elephant elephant = new Elephant(850,6.9,"Klain",35);
        Horse horse = new Horse(89,3.5,"Madrid",19);

        //  Create worker;
        Worker worker = new Worker();

        //  Create two types of food: grass and meat;
        Food meat = new Meat("Chicken", 500);
        Food grass = new Grass("Fresh grass", 100);

        //  Feed the animal with different food.
        worker.feed(lion,meat);
        worker.feed(lion,grass);
        worker.feed(carp,meat);
        worker.feed(carp,grass);
        worker.feed(bear,meat);
        worker.feed(bear,grass);
        worker.feed(elephant,meat);
        worker.feed(elephant,grass);
        worker.feed(horse,meat);
        worker.feed(horse,grass);


        // Call a method 'castVote' on objects lion and duck (only they implement 'Voice')
        worker.getVoice(lion);
        worker.getVoice(duck);

        Swim[] swimAnimals = {bear,carp,duck};
        for(Swim animal: swimAnimals) {
            animal.swimming();
        }

        System.out.println(lion.getSatiety());
        lion.runNow();
        System.out.println(lion.getSatiety());
    }
}
