package animals;

public class Bear extends Carnivorous implements Run, Swim {
    public Bear(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void runNow() {

        System.out.println("Bear is running");
        setSatiety(getSatiety() - 7);
    }

    @Override
    public void swimming() {

        System.out.println("Bear is swimming! Dangerous!");
        setSatiety(getSatiety() - 10);
    }
}
