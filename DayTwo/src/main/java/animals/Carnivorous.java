package animals;

import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void eat(Food food) {
        if(food instanceof Meat) {
            System.out.println("That's meat is delicious!");
            if(food.getCalories() > 200)
            this.setSatiety(getSatiety() + 13);
            else {
                this.setSatiety(getSatiety() + 5);
            }

        } else {
            System.out.println("ERROR. " + this.getName() + " doesn't eat food like that!");
        }
    }
}
