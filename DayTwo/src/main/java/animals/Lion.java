package animals;

public class Lion extends Carnivorous implements Run, Voice {

    public Lion(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void runNow() {
        System.out.println("Lion is gonna start running.");
        setSatiety(getSatiety() - 6);
    }

    @Override
    public String castVote() {
        return "Grrrar";
    }
}
