package animals;

public class Horse extends Herbivore implements Run {

    public Horse(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void runNow() {

        System.out.println("Horse is fast! He's running!");
        setSatiety(getSatiety() - 2);
    }
}
