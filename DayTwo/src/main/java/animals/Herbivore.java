package animals;


import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    public Herbivore(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void eat(Food food) {
        if(food instanceof Grass) {
            System.out.println("Nice food");
            if(food.getCalories() > 50)
                this.setSatiety(getSatiety() + 5);
            else {
                this.setSatiety(getSatiety() + 2);
            }
        } else {
            System.out.println("ERROR. " + this.getName() + "  doesn't eat food like that");
        }
    }
}
