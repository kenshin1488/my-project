package animals;

public interface Voice {

    String castVote();
}
