package animals;

public class Duck extends Carnivorous implements Fly, Swim, Voice {

    public Duck(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void flying() {
        System.out.println("Duck is gonna be start flying");
        setSatiety(getSatiety() - 4);
    }

    @Override
    public String castVote() {
        return "Kryaa";
    }

    @Override
    public void swimming() {

        System.out.println("Duck is swimming");
        setSatiety(getSatiety() - 3);
    }


}
