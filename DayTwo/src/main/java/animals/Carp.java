package animals;

public class Carp extends Herbivore implements Swim {

    public Carp(int weight, double dangerous, String name, int satiety) {
        super(weight, dangerous, name, satiety);
    }

    @Override
    public void swimming() {
        System.out.println("Carp is swimming");
        setSatiety(getSatiety() - 1);
    }
}
